---

# Green Vs Red Game
The following Java project is the Green vs Red game.

Author: Ivan Takev

The game works by getting coordinates like a coordinate system (x, y) for the size of the grid.

You fill the grid by user input.

Then, you add your desired cell coordinates and amount of generations and you will get the times your selected cell is green.
    
 * Please note that since we are using x and y, that is width and height of the grid and the coordinates for the cells in the grid.

    This means that x = 1, y = 0 in the grid would be grid[0][1] in an array. 

  * I am using @width and @height as parameters instead of x and y in order to simplify
  
  
To start the game, please call playGame() method of a Game/GameImpl object.

## Classes:
### Application
Class for calling the main method.

### ConsolePrinterImpl
Class for printing to the console.

### GridModifierImpl
Class for modifying the grid

### InputReaderImpl
Class for reading input from console.

### ParserImpl
Class for parsing user input

### GameImpl
Class containing the actual game

###ValidatorImpl
Class containing validation







---