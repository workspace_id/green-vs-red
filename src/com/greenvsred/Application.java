package com.greenvsred;

import com.greenvsred.impl.*;
import com.greenvsred.interfaces.ConsolePrinter;
import com.greenvsred.interfaces.Game;

import java.util.Scanner;

/**
 * Class just for main method for starting application.
 * Please look at Game Class in order to get more information.
 */

public class Application {
    //Main method for playing game
    public static void main(String[] args) {
        //Passing objects (dependencies)
        ConsolePrinter printer = new ConsolePrinterImpl();
        Game game = new GameImpl(printer, new InputReaderImpl(new Scanner(System.in), printer, new ValidatorImpl(), new ParserImpl()), new GridModifierImpl());
        game.playGame();
    }
}
