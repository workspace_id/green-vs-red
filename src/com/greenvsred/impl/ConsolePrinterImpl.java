package com.greenvsred.impl;

import com.greenvsred.interfaces.ConsolePrinter;


/**
 * Class for printing output to the console
 */

public class ConsolePrinterImpl implements ConsolePrinter {
    //Prints out greeting
    public void printGreeting() {
        println("Welcome to Green vs Red!");
    }

    //Prints out the current grid
    public void printGrid(int[][] grid) {
        for (int[] ints : grid) {
            for (int k = 0; k < grid[0].length; k++) {
                System.out.print(ints[k]);
                if (k == grid[0].length - 1) {
                    System.out.println();
                }
            }
        }
        System.out.println();
    }

    public void printGeneration(int N, int[][] grid) {
        println("Generation: " + (N+1));
        printGrid(grid);
    }

    public void printResult(int width, int height, int timesGreen) {
        println("Your cell " + width + "," + height + " was green " + timesGreen + " times");
    }

    //Prints out thanks
    public void printThanks() {
        println("Thank you for playing!");
    }

    //Prints out line
    public void println(String s) {
        System.out.println(s);
    }

    //Prints out on same line
    public void print(String s) {
        System.out.print(s);
    }
}
