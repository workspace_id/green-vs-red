package com.greenvsred.impl;

import com.greenvsred.interfaces.ConsolePrinter;
import com.greenvsred.interfaces.Game;
import com.greenvsred.interfaces.GridModifier;
import com.greenvsred.interfaces.InputReader;

/**
 * The following Java class is the Green vs Red game.
 * @author Ivan Takev
 *
 * The game works by getting coordinates like a coordinate system (x, y) for the size of the grid.
 * You fill the grid by user input.
 * Then, you add your desired cell coordinates and amount of generations and you will get the times your selected cell is green.
 *
 * Please note that since we are using x and y, that is width and height of the grid and the coordinates for the cells in the grid.
 * This means that x = 1, y = 0 in the grid would be grid[0][1] in an array.
 * I am using @width and @height as parameters instead of x and y in order to simplify
 *
 */

public class GameImpl implements Game {
    private int[][] grid;
    private int width;
    private int height;
    private int x1;
    private int y1;
    private int N;
    //Using interfaces for DI
    private InputReader inputReader;
    private ConsolePrinter printer;
    private GridModifier gridModifier;

    //Constructor
    public GameImpl(ConsolePrinter printer, InputReader inputReader, GridModifier gridModifier) {
        this.printer = printer;
        this.inputReader = inputReader;
        this.gridModifier = gridModifier;
    }

    private void setWidth(int width) {
        this.width = width;
    }

    private void setHeight(int height) {
        this.height = height;
    }

    private void setGrid(int[][] grid) {
        this.grid = grid;
    }

    private void setX1(int x1) {
        this.x1 = x1;
    }

    private void setY1(int y1) {
        this.y1 = y1;
    }


    private void setN(int N) {
        this.N = N;
    }

    //Shows the amount of time that the cell was green
    public void printAmountTimesGreen() {
        int timesGreen = 0;
        for (int i = -1; i < N; i++) {
            printer.printGeneration(i, grid);

            if (gridModifier.isGreen(x1, y1, grid)) {
                timesGreen++;
            }
            setGrid(gridModifier.generateNextGeneration(grid));
        }

        printer.printResult(x1, y1, timesGreen);
    }

    //Asks user if he wants to play again
    public void playAgain() {
        if (!inputReader.yesOrNo("Play again?")) {
            printer.printThanks();
            inputReader.close();
            System.exit(0);
        }
    }

    //Gets grid dimensions from user input
    public void getGridDimensionsFromUserInput() {
        setWidth(inputReader.readValidIntFromConsole("Please enter x dimension (Grid width): ", 2, 1000));
        setHeight(inputReader.readValidIntFromConsole("Please enter y dimension (Grid height): ", width, 1000));
    }

    //Generates and fills grid from user input
    public void generateAndFillGridFromUserInput() {
        setGrid(inputReader.fillGridFromConsole(gridModifier.generateNewGrid(width, height)));
    }

    //Sets the cell we are looking for
    public void setCellWeAreLookingFor() {
        setX1(inputReader.readValidIntFromConsole("Enter x value of the cell you are looking for: ", 0, width - 1));
        setY1(inputReader.readValidIntFromConsole("Enter y value of the cell you are looking for: ", 0, height - 1));
    }

    //Sets the number of generations
    public void setNumberOfGenerations() {
        setN(inputReader.readValidIntFromConsole("Enter size of N amount of generations: ", 1, 1000));
    }

    //Runs the game
    public void playGame() {
        printer.printGreeting();
        while (true) {
            //Getting grid dimensions from user input
            getGridDimensionsFromUserInput();

            //Generating grid by getting its dimensions and content from console
            generateAndFillGridFromUserInput();

            //Setting cell that we are looking for
            setCellWeAreLookingFor();

            //Setting number of generations
            setNumberOfGenerations();

            //Showing how many times that cell was green
            printAmountTimesGreen();

            //Play again?
            playAgain();
        }
    }
}




