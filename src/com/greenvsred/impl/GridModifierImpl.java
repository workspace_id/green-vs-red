package com.greenvsred.impl;

import com.greenvsred.interfaces.GridModifier;

/**
 * Class for modifying the grid of the game.
 *
 * Please note that since we are using x and y, that is width and height of the grid and the coordinates for the cells in the grid.
 *  * This means that x = 1, y = 0 in the grid would be grid[0][1] in an array.
 *  * I am using @width and @height as parameters instead of x and y in order to simplify.
 */

public class GridModifierImpl implements GridModifier {

    //Returns new grid based on the width and height values
    public int[][] generateNewGrid (int width, int height) {
        return new int[height][width];
    }

    //Returns value of the grid location (x and y are fed in a format like a coordinate system)
    public int getGridLocationValue(int width, int height, int[][] grid) {
        try {
            return grid[height][width];
        } catch (ArrayIndexOutOfBoundsException e) {
            //Returning 0 as position in the array does not exist
            return 0;
        }
    }

    //Sums all cells surrounding the coordinates to prepare for the calculation of the value for the new generation
    public int sumSurroundingCells(int width, int height, int[][] grid) {
        int sum = 0;

        for (int i=height-1; i<height+2; i++) {
            for (int k=width-1; k<width+2; k++) {
                if (!(i==height && k==width)) {
                    sum = getGridLocationValue(k, i, grid) + sum;
                }
            }
        }

        return sum;
    }

    //Returns new cell value for the new generation
    public int generateNewValue(int width, int height, int[][] grid) {
        int number = sumSurroundingCells(width, height, grid);
        int fieldValue = grid[height][width];
        int newValue = fieldValue;

        if (fieldValue == 0) {
            if (number == 3 || number == 6) {
                newValue = 1;
            }
        } else if (fieldValue==1) {
            if (number == 0 || number == 1 || number == 4 || number == 5 || number == 7 || number == 8) {
                newValue = 0;
            }
        }

        return newValue;
    }

    //Updates the grid as the next generation
    public int[][] generateNextGeneration(int[][] grid) {
        //Can be further optimized to reuse a temp array instead of instantiating new ones
        int[][] nextGeneration = new int[grid.length][grid[0].length];
        for (int i=0; i<grid.length; i++) {
            for (int k = 0; k < grid[0].length; k++) {
                nextGeneration[i][k] = generateNewValue(k, i, grid);
            }
        }
        return nextGeneration;
    }

    //Prints out the times that the selected cell is green
    public boolean isGreen(int width, int height, int[][] grid) {
            return (grid[height][width] == 1);
    }
}
