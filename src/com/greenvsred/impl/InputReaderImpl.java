package com.greenvsred.impl;

import com.greenvsred.interfaces.ConsolePrinter;
import com.greenvsred.interfaces.InputReader;
import com.greenvsred.interfaces.Parser;
import com.greenvsred.interfaces.Validator;

import java.util.Scanner;


/**
 * Class for reading input from the command line.
 * Scanner is final so I cannot extend it and have it implement an interface.
 * This can be further optimized by adding a Class that performs the same instead and have it be used with an interface.
 */

public class InputReaderImpl implements InputReader {
    //Can be further optimized to use a separate Class with an interface for DI instead of Scanner
    private Scanner scan;
    private ConsolePrinter printer;
    private Validator validator;
    private Parser parser;

    //I could just instantiate it here but I will leave it in the constructor for now.
    public InputReaderImpl(Scanner scan, ConsolePrinter printer, Validator validator, Parser parser) {
        this.scan = scan;
        this.printer = printer;
        this.validator = validator;
        this.parser = parser;
    }

    //Used by Scanner
    public void close() {
        scan.close();
    }

    //Reads valid int input from console, will not return until input is valid
    public int readValidIntFromConsole(String message, int min, int max) {
        while (true) {
            printer.print(message);
            try {
                int n = parser.parseInt(scan.nextLine());
                if (validator.validateNumberIsBetweenInclusive(n, min, max)) {
                    return n;
                }
                printer.print("Error, please enter a number between " + min + " and " + max + "\n");
            } catch (NumberFormatException e) {
                printer.print("Error, please enter an int \n");
            }
        }
    }

    //Fills grid from console input
    public int[][] fillGridFromConsole(int[][] grid) {
        for (int i = 0; i<grid.length; i++) {
            for (int k = 0; k<grid[0].length; k++) {
                grid[i][k] = readValidIntFromConsole("Please enter a binary number to fill cell at: " + k + "," + i, 0, 1);
            }
        }
        return grid;
    }

    //Returns true for yes and false for no
    public boolean yesOrNo(String message) {
        while (true) {
            printer.println(message);
            String s = scan.nextLine();
            if (s.equals("yes") || s.equals("no")) {
                return s.equals("yes");
            }
            printer.println("Please answer with yes or no");
        }
    }

}
