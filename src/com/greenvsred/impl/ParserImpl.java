package com.greenvsred.impl;

import com.greenvsred.interfaces.Parser;

/**
 * Class for parsing String to int.
 */
public class ParserImpl implements Parser {
    //Parses String to int
    public int parseInt(String s) {
        return Integer.parseInt(s);
    }

}
