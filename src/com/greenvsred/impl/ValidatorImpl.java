package com.greenvsred.impl;

import com.greenvsred.interfaces.Validator;

/**
 * Class for validating ints.
 */
public class ValidatorImpl implements Validator {
    //Checks if a number is between the 2 values inclusively
    public boolean validateNumberIsBetweenInclusive(int n, int min, int max) {
        return (n >= min && n <= max);
    }
}
