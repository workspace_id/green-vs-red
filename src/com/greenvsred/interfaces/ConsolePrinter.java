package com.greenvsred.interfaces;

public interface ConsolePrinter {
    //Prints Greeting
    void printGreeting();

    //Prints out the current grid
    void printGrid(int[][] grid);

    //Prints out generation
    void printGeneration(int N, int[][] grid);

    //Prints times green
    void printResult(int width, int height, int timesGreen);

    //Prints thank you message
    void printThanks();

    //Prints out line
    void println(String s);

    //Prints out on same line
    void print(String s);
}
