package com.greenvsred.interfaces;

public interface Game {
    //Shows the amount of time that the cell was green
    void printAmountTimesGreen();

    //Asks user if he wants to play again
    void playAgain();

    //Gets grid dimensions from user input
    void getGridDimensionsFromUserInput();

    //Generates and fills grid from user input
    void generateAndFillGridFromUserInput();

    //Sets the cell we are looking for
    void setCellWeAreLookingFor();

    //Sets the number of generations
    void setNumberOfGenerations();

    //Runs the game
    void playGame();
}




