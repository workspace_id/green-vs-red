package com.greenvsred.interfaces;

public interface GridModifier {
    //Returns new grid based on the width and height values
    int[][] generateNewGrid (int width, int height);

    //Returns value of the grid location (x and y are fed in a format like a coordinate system)
    int getGridLocationValue(int width, int height, int[][] grid);

    //Sums all cells surrounding the coordinates to prepare for the calculation of the value for the new generation
    int sumSurroundingCells(int width, int height, int[][] grid);

    //Returns new cell value for the new generation
    int generateNewValue(int width, int height, int[][] grid);

    //Updates the grid as the next generation
    int[][] generateNextGeneration(int[][] grid);

    //Returns true if cell is green
    boolean isGreen(int width, int height, int[][] grid);
}
