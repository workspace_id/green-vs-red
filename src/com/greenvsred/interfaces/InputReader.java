package com.greenvsred.interfaces;

public interface InputReader {

    //Reads valid int input from console, will not return until input is valid
    int readValidIntFromConsole(String message, int min, int max);

    //Fills grid from console input
    int[][] fillGridFromConsole(int[][] grid);

    //Returns true for yes, false for no
    boolean yesOrNo(String message);

    //Method for closing scanner
    void close();

}
