package com.greenvsred.interfaces;

public interface Validator {
    //Checks if a number is between the 2 values inclusively
    boolean validateNumberIsBetweenInclusive(int n, int min, int max);
}
